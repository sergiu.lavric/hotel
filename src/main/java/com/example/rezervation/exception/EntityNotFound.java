package com.example.rezervation.exception;

public class EntityNotFound extends Exception{
    public EntityNotFound(String msg) {
        super(msg);
    }
}
