package com.example.rezervation.Dto;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@NoArgsConstructor
@Getter
@Setter
public class FoodDto {
    private UUID uuid;

    private boolean isIncluded;
    private  String breakfast;
    private String lunch;
    private String dinner;


}
