package com.example.rezervation.Dto;

//import com.example.rezervation.model.DaysInHotel;
import com.example.rezervation.model.Booking;
import com.example.rezervation.model.FoodIncluded;
import com.example.rezervation.model.Hotel;
import com.example.rezervation.model.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class RoomDto {
    private UUID uuid;
    private Integer roomNumber;
    private Boolean isOcupied;
    private Hotel hotel;
    private Double price ;
    private String roomType;

    private  List<FoodIncluded> foodIncludedList;
    private Set<User> users;
    private List<Booking> bookings;



}


