package com.example.rezervation.Dto;

import com.example.rezervation.model.Room;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@NoArgsConstructor
@Getter
@Setter
public class BookingDto {
    private UUID uuid;

    private Double checkIn;
    private Double checkOut;

}
