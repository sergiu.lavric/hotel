package com.example.rezervation.Dto;

import com.example.rezervation.model.Room;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.UUID;

@NoArgsConstructor
@Setter
@Getter
public class HotelDto {
    private UUID uuid;
    private String name;
    private Integer capacity;
    private List<Room> roomList;

}
