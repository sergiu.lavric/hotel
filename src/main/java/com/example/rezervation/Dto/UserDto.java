package com.example.rezervation.Dto;

//import com.example.rezervation.model.DaysInHotel;
import com.example.rezervation.model.FoodIncluded;
import com.example.rezervation.model.Room;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class UserDto {
    private UUID uuid;
    private String name;
    private Integer numberOfGuest;
    private Set<Room> rooms;


}

