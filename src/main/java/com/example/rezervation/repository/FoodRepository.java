package com.example.rezervation.repository;

import com.example.rezervation.model.FoodIncluded;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface FoodRepository extends JpaRepository<FoodIncluded , UUID> {
}
