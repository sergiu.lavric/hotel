package com.example.rezervation.service;


import com.example.rezervation.Dto.HotelDto;
import com.example.rezervation.model.Hotel;
import com.example.rezervation.repository.BookingRepository;
import com.example.rezervation.repository.HotelRepository;
import com.example.rezervation.repository.RoomRepository;
import com.example.rezervation.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.UUID;

@Service
public class HotelService {
    private HotelRepository hotelRepository;
    private RoomRepository roomRepository;
    private UserRepository userRepository;


    @Autowired
    public HotelService(HotelRepository hotelRepository, RoomRepository roomRepository, UserRepository userRepository) {
        this.hotelRepository = hotelRepository;
        this.roomRepository = roomRepository;
        this.userRepository = userRepository;
    }

    public Hotel addHotel(HotelDto hotelDto) {
        Hotel hotel = new Hotel ();
        hotel.setUuid (hotelDto.getUuid ());
        hotel.setCapacity (hotelDto.getCapacity ());
        hotel.setName (hotelDto.getName ());
        hotel.setRoomList (hotelDto.getRoomList ());
        hotelRepository.save (hotel);
        return hotel;
    }

    public List<Hotel> getAListOfHotel() {
        List<Hotel> hotelList= hotelRepository.findAll ();
        return hotelList;
    }

    public Hotel getHotels(UUID uuid) {
      return  hotelRepository.findById (uuid).orElseThrow (()->new EntityNotFoundException ("not found"));

    }



}
