package com.example.rezervation.service;



import com.example.rezervation.repository.RoomRepository;
import com.example.rezervation.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UserService {
    private RoomRepository roomRepository;
    private UserRepository userRepository;

    @Autowired
    public UserService(RoomRepository roomRepository, UserRepository userRepository) {
        this.roomRepository = roomRepository;
        this.userRepository = userRepository;
    }


}
