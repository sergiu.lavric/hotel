package com.example.rezervation.service;
import com.example.rezervation.Dto.BookingDto;
import com.example.rezervation.Dto.FoodDto;
import com.example.rezervation.Dto.RoomDto;
import com.example.rezervation.Dto.UserDto;
import com.example.rezervation.model.*;
import com.example.rezervation.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class RoomService {
    private final HotelRepository hotelRepository;
    private final RoomRepository roomRepository;
    private final UserRepository userRepository;
    private final BookingRepository bookingRepository;
    private FoodRepository foodRepository;


    @Autowired
    public RoomService(HotelRepository hotelRepository, RoomRepository roomRepository, UserRepository userRepository, BookingRepository bookingRepository,FoodRepository foodRepository) {
        this.hotelRepository = hotelRepository;
        this.roomRepository = roomRepository;
        this.userRepository = userRepository;
        this.bookingRepository = bookingRepository;
        this.foodRepository= foodRepository;

    }


    public Room addRoom(UUID uuid, RoomDto roomDto) {
        Hotel hotel = hotelRepository.findById (uuid).orElseThrow (() -> new EntityNotFoundException ("not found"));
        Room room = new Room ();
        room.setUuid (roomDto.getUuid ());
        room.setHotel (hotel);
        room.setRoomNumber (roomDto.getRoomNumber ());
        room.setIsOcupied (roomDto.getIsOcupied ());
        room.setBookings (roomDto.getBookings ());
        room.setPrice (roomDto.getPrice ());
        room.setRoomType (roomDto.getRoomType ());
        room.setFoodIncludedList (roomDto.getFoodIncludedList ());
        roomRepository.save (room);
        return room;
    }

    public Room addBooking(UUID uuid, BookingDto bookingDto,RoomDto roomDto,FoodDto foodDto) {
        Room room = roomRepository.findById (uuid).orElseThrow (() -> new EntityNotFoundException ("not found"));
        Booking booking = new Booking ();
        FoodIncluded foodIncluded = new FoodIncluded ();
        booking.setRoom (room);
        booking.setCheckIn (bookingDto.getCheckIn ());
        booking.setCheckOut (bookingDto.getCheckOut ());
        room.setIsOcupied (true);
        room.setFoodIncludedList (roomDto.getFoodIncludedList ());

        bookingRepository.save (booking);
        return room;
    }


    public List<Room> verifyRooms() {
        List<Room> roomList = roomRepository.findAll ();
        List<Room> emptyRoom = new ArrayList<> ();
        for (Room room : roomList) {
            if (!room.getIsOcupied ()) emptyRoom.add (room);
        }
        return emptyRoom;
    }

    //
    public List<Room> addBookingToEmptyRoom(RoomDto roomDto, Integer roomNumber, UUID uuid) {
        List<Room> roomList = roomRepository.findAll ();
        List<Room> emptyRooms = new ArrayList<> ();
        for (Room room : roomList) {
            if (!room.getIsOcupied ()) {
                Hotel hotel = new Hotel ();

                Room newRoom = roomRepository.getOne (uuid);
                newRoom.setRoomNumber (roomNumber);
                newRoom.setIsOcupied (roomDto.getIsOcupied ());
                newRoom.setHotel (roomDto.getHotel());
                hotel.getRoomList ().add (newRoom);


                roomRepository.save (newRoom);
                emptyRooms.add (room);


            }
        }
        return emptyRooms;
    }
    public List<Room> ifClientLeave(BookingDto bookingDto,UUID uuid) {
        List<Room> roomsList = roomRepository.findAll ();
        List<Room> ocupiedRooms = new ArrayList<> ();

        for(Room rooms : roomsList) {
            if(rooms.getIsOcupied ()) {
                 Room room = roomRepository.getOne (uuid);
                 Booking booking = new Booking ();
                 booking.setCheckIn (bookingDto.getCheckIn ());
                 booking.setCheckOut (bookingDto.getCheckOut ());
                 room.getBookings ().add (booking);
                 room.setIsOcupied (false);
                 roomRepository.save (room);
                 ocupiedRooms.add (room);


            }
        }
        return ocupiedRooms;
    }

    public String addUser(UserDto userDto, UUID uuid) throws EntityNotFoundException {
        Room room = roomRepository.findById (uuid).orElseThrow (() -> new EntityNotFoundException ("not found"));
        User users = new User ();

        users.setName (userDto.getName ());
        users.setNumberOfGuest (userDto.getNumberOfGuest ());
        users.setUuid (userDto.getUuid ());
        users.setRooms (userDto.getRooms ());
          room.getUsers ().add (users);

        userRepository.save (users);


        return "added user ";
    }

    public String mealsIncluded ( UUID uuid ,FoodDto foodDto) {
        Room room = roomRepository.findById (uuid).orElseThrow (()->new EntityNotFoundException ("not found"));

        FoodIncluded foodIncluded = new FoodIncluded ();
        foodIncluded.setIncluded (foodDto.isIncluded ());
        room.getFoodIncludedList ().add (foodIncluded);

   if(room.getPrice ()< 50 && foodIncluded.isIncluded ()) {
      return "client has only break-fast included";
   }else if(room.getPrice ()>=50 && foodIncluded.isIncluded () ){
       return ("client has all meals included" ) ;
   } else  {
       return "client dosent have meals included";
   }
    }






}


//    public List<Room> addUser(UserDto userDto, RoomDto roomDto, UUID uuid) {
//        List<Room> roomList = roomRepository.findByIsOcupied (false);
//
//        if(roomList==null) {
//            Room newRoom = new Room ();
//            User user= new User ();
//            newRoom.getUsers ().add (user);
//            newRoom.setRoomNumber (roomDto.getRoomNumber ());
//            newRoom.s(roomDto.getCheckOut ());
//            newRoom.setIsOcupied (true);
//            user.setRooms (userDto.getRooms ());
//            user.setNumberOfGuest (userDto.getNumberOfGuest ());
//            user.setName (userDto.getName ());
//
//            return (List<Room>) roomRepository.save (newRoom);
//        }
//        return roomList;


//    public Optional<Room> getRoom(UUID id) {
//        List<Room> room = roomRepository.findAllById (id);
//        return room;
//    }

//    public Room addRoomIfEmpty(UUID uuid) {
//        Room room = new Room ();
//        List<Room> roomList = new ArrayList<> ();
//        for (int i = 0; i == roomList.size (); i++) {
//            i
//        }
//
//    }

//public  String setRoom () {
//        Room room = new Room ();
//
//}


