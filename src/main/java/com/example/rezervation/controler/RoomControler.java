package com.example.rezervation.controler;


import com.example.rezervation.Dto.BookingDto;
import com.example.rezervation.Dto.FoodDto;
import com.example.rezervation.Dto.RoomDto;
import com.example.rezervation.Dto.UserDto;
import com.example.rezervation.exception.EntityNotFound;
import com.example.rezervation.service.RoomService;
import com.example.rezervation.util.HttpStatusHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/room")
public class RoomControler {

    private final RoomService roomService;

    @Autowired
    public RoomControler(RoomService roomService) {
        this.roomService = roomService;
    }

    //
    @PostMapping("/available-room/{uuid}")

    public ResponseEntity addRoom(@RequestBody RoomDto roomDto, @PathVariable UUID uuid) throws EntityNotFound {
        try {
            return HttpStatusHelper.success ("added room", roomService.addRoom (uuid, roomDto));
        } catch (Exception e) {
            return HttpStatusHelper.commonErrorMethod (e);
        }
    }

    @PostMapping("/save-booking/{uuid}")
    public ResponseEntity addRoom(@RequestBody BookingDto bookingDto, @PathVariable UUID uuid,RoomDto roomDto) throws EntityNotFound {
        try {
            return HttpStatusHelper.success ("added booking", roomService.addBooking (uuid, bookingDto,roomDto));
        } catch (Exception e) {
            return HttpStatusHelper.commonErrorMethod (e);
        }
    }


        @PostMapping("/reserve-room/{uuid}/room-number")
    public ResponseEntity reserveRoom(@RequestBody RoomDto roomDto, @RequestParam Integer roomNumber  , @PathVariable UUID uuid) {
        try {
            return HttpStatusHelper.success ("reserve room ", roomService.addBookingToEmptyRoom (roomDto, roomNumber, uuid));
        } catch (Exception e) {
            return HttpStatusHelper.commonErrorMethod (e);
        }
    }


    @PostMapping("/user-to-room/{uuid}")
    public ResponseEntity addUser(@RequestBody UserDto userDto, @PathVariable UUID uuid) {
        try {
            return HttpStatusHelper.success ("added user to room", roomService.addUser (userDto, uuid));
        } catch (Exception e) {
            return HttpStatusHelper.commonErrorMethod (e);
        }
    }
    @PostMapping("/user-leaves/{uuid}")
    public ResponseEntity userLeaves(@RequestBody BookingDto bookingDto,@PathVariable UUID uuid)  {
        try {
            return HttpStatusHelper.success (" user leaves ", roomService.ifClientLeave (bookingDto,uuid));
        } catch (Exception e) {
            return HttpStatusHelper.commonErrorMethod (e);
        }
    }
    @GetMapping("/if-room-available")
    public ResponseEntity getRooms() {
        try {
            return HttpStatusHelper.success ("verify a list of free rooms ", roomService.verifyRooms ());
        } catch (Exception e) {
            return HttpStatusHelper.commonErrorMethod (e);
        }
    }
    @PostMapping("/food-included/{uuid}")
    public ResponseEntity foodIncluided(  @PathVariable UUID uuid ,@RequestBody FoodDto foodDto) {
        try {
            return HttpStatusHelper.success ("is food included", roomService.mealsIncluded ( uuid,foodDto));
        } catch (Exception e) {
            return HttpStatusHelper.commonErrorMethod (e);
        }
    }
}
