package com.example.rezervation.controler;


import com.example.rezervation.Dto.HotelDto;
import com.example.rezervation.exception.EntityNotFound;
import com.example.rezervation.service.HotelService;
import com.example.rezervation.util.HttpStatusHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/hotel")
public class HotelControler {

    private HotelService hotelService;

    @Autowired
    public HotelControler(HotelService hotelService) {
        this.hotelService = hotelService;
    }


@PostMapping("/new-hotel")
    public ResponseEntity addHotel(@RequestBody HotelDto hotelDto) throws EntityNotFound {
        try {
            return HttpStatusHelper.success ("added hotel", hotelService.addHotel (hotelDto));

        } catch (Exception e) {
            return HttpStatusHelper.commonErrorMethod (e);

        }
    }

    @GetMapping("/list-all-hotels")
    ResponseEntity getAllHotels() throws EntityNotFound {
        try {
            return HttpStatusHelper.success ("get a list of hotels", hotelService.getAListOfHotel ());

        } catch (Exception e) {
            return HttpStatusHelper.commonErrorMethod (e);

        }

    }
    @GetMapping("/get-hotel/{uuid}")
    public ResponseEntity getHotel(@PathVariable UUID uuid) throws EntityNotFound {
        try {
            return HttpStatusHelper.success ("get a single hotel", hotelService.getHotels(uuid ));

        } catch (Exception e) {
            return HttpStatusHelper.commonErrorMethod (e);

        }
    }
}
