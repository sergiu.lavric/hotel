package com.example.rezervation.util;

import com.example.rezervation.exception.EntityNotFound;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.Map;

public class HttpStatusHelper {
    public static ResponseEntity<Object> success(String objectName, Object object) {
        Map<String, Object> map = new HashMap<> ();
        map.put(objectName, object);
        return new ResponseEntity<>(map, org.springframework.http.HttpStatus.OK);
    }

    public static ResponseEntity commonErrorMethod(Exception e) {
        e.printStackTrace();
        Map<String, String> map = new HashMap<>();
        map.put("message", e.getMessage());

        if (e instanceof EntityNotFound) {
            return new ResponseEntity(map, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity(map, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
